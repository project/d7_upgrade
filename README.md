# Introduction
The D7 Upgrade module provides an analysis and reporting framework for Drupal 7 sites.
The module comprises of a core analysis engine and reporting interface, and several
submodules for analysing different aspects of your site including:
* Content types
* Fields
* Modules
* Taxonomies
* Users
* Views

The module is designed to be:
* Extensible
  * Uses a plugin-type architecture, allowing you to create your own analysis submodules
* Flexible
  * Gives you the freedom to define the inner workings of your own analyses & reports
* Scalable
  * Utilises a 2-step batch analysis process
    * Analysis stage 1 is responsible for identifying items and calculations. This is essentially forming
    a queue of granular analysis items for processing during stage 2
    * Analysis stage 2 is responsible for executing the fine-grained analysis and statistical calculations identified in
    stage 1.

# Requirements
The module has no specific requirements. Each submodule may contain dependencies that relate to the functional
area they intend to analyse, for example d7_upgrade_views requires the Views module to be enabled.

# Installation
* Download the module from https://www.drupal.org/project/d7_upgrade as you would any other module
* On the Modules page, enable:
  * Drupal 7 Upgrade (d7_upgrade)
  * 1 or many analysis submodules, e.g. Drupal 7 Upgrade Content Types (d7_upgrade_content_types)

# Usage
Once the module and analysis submodule(s) have been installed (see Installation):
* Navigate to the reporting dashboard at /admin/reports/d7_upgrade
* Click "Run analysis"
  * Click "Run" to start the 2-step analysis process, then wait until both parts of the analysis have completed
    * Do not attempt to manually refresh the progress pages
* Once both stages of the analysis has run, you will be redirected back to the reporting dashboard where you can:
  * Browse summary reports
  * Access links fo full detailed analysis reports
  * Download individual analysis reports in CSV format
  * Download all analysis reports in CSV format (via the 'Download all reports' button)

# Configuration
* The only configuration needed is to enable one or more analysis submodules
