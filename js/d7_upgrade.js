/**
 * @file
 * Javascript behaviors for D7 Upgrade.
 */
(function ($) {
  Drupal.behaviors.d7_upgrade = {
    attach: function(context, settings) {
      $("#-d7-upgrade-analysis-form", context).once("d7_upgrade").submit();
    }
  }
}(jQuery));
