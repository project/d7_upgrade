<?php

/**
 * @file
 * Class D7UpgradeAnalysisAbstract.
 */

/**
 * Class D7UpgradeAnalysisAbstract.
 */
abstract class D7UpgradeAnalysisAbstract {

  protected $items = array();

  protected $analysisData = array();

  protected $summaryStats = array();

  protected $id = NULL;

  public function __construct() {
    $this->id = $this->getId();
  }

  /**
   * Define a unique analysis ID for your analysis class.
   *
   * Example:
   *  return 'my_unique_analysis';
   *
   * @return string
   */
  abstract public function getId();

  /**
   * Define a human readable name for your analysis.
   *
   * Example:
   *  return 'My Analysis Title';
   *
   * @return string
   */
  abstract public function getLabel();

  /**
   * This method is called during the first stage of the batch analysis process.
   *
   * Implementations of this method should only attempt to identify items for detailed analysis in analyseItem().
   *
   * Example:
   *  $this->items = array('item1', 'item2');
   *  $this->saveItems();
   *
   * @return mixed
   */
  abstract public function identifyItems();

  /**
   * This method is called during the second stage of the batch analysis process.
   *
   * For each iteration of the batch, one item identified in identifyItems() will be passed to the method for
   * detailed analysis.
   *
   * Example:
   *  $this->analysisData['key'] = $item;
   *
   *  $object = example_get_data($item);
   *  $this->analysisData['my_value'] = count($object->values);
   *
   *  // Save analysis data back to the database for use in renderReport().
   *  $this->saveAnalysisData();
   *
   * @param $item
   * @return mixed
   */
  abstract public function analyseItem($item);

  /**
   * Method for rendering reports from data captured in analyseItem().
   *
   * Example:
   *  // Get data stored during analyseItem().
   *  $data = $this->getAnalysisData();
   *
   *  // Construct table header.
   *  $header = array(
   *    'Key',
   *    'Value'
   *  );
   *
   *  // Construct table rows.
   *  $rows = array();
   *  foreach ($data as $k => $v) {
   *    $rows[] = array(
   *      $v->key,
   *      $v->my_value
   *    );
   *  }
   *
   *  // Return an array of headers and rows.
   *  return array(
   *    'header' => $header,
   *    'rows' => $rows
   *  );
   *
   * @return array
   */
  abstract public function renderReport();

  /**
   * Identify all stats registered to this analysis and save references to them in the database.
   */
  public function identifyStats() {
    $stats_classes = $this->getStatisticsClasses();
    foreach ($stats_classes as $class) {
      $stat = new $class();
      $stat->saveItem();
    }
  }

  /**
   * Identify classes that implement statistics.
   */
  private function getStatisticsClasses() {
    $return = array();
    $my_class = get_class($this);

    // Invoke custom hook to identify registered stat classes in the codebase.
    $stat_classes = module_invoke_all('register_statistic');

    // Check each class exists.
    foreach ($stat_classes as $stat_class) {
      if (class_exists($stat_class)) {
        // Check that the stat is registered to this class.
        $stat = new $stat_class();
        $analysis_class = $stat->getAnalysisClass();

        // Add the stat to an array.
        if ($analysis_class == $my_class && !in_array($stat_class,  $return)) {
          array_push($return, $stat_class);
        }
      }
    }
    return $return;
  }

  /**
   * Function to render a report summary statistics for the analysis.
   */
  public function renderStatsReport() {
    $stats_classes = $this->getStatisticsClasses();
    $rows = array();
    foreach ($stats_classes as $class) {
      $stat = new $class();
      $value = $stat->getStoredValue();
      $description = $stat->getDescription();
      if (isset($value)) {
        $rows[] = array(
          $stat->label,
          $value,
          $description
        );
      }
    }

    if (empty($rows)) {
      return NULL;
    }
    else {
      return theme('table', array(
        'rows' => $rows,
      ));
    }
  }

  /**
   * Store analysis items in the database.
   */
  protected function saveItems() {
    $class = get_class($this);
    foreach ($this->items as $item) {
      try {
        db_insert('d7_upgrade_analysis_items')
          ->fields(array(
            'item_type' => 'line-item',
            'item_class' => $class,
            'item_data_key' => $item,
          ))
          ->execute();
      }
      catch (\Exception $exception) {
        watchdog_exception('type', $exception);
      }
    }
  }

  /**
   * Store JSON analysis data in the database.
   */
  protected function saveAnalysisData() {
    $json = json_encode($this->analysisData);
    $class = get_class($this);
    try {
      db_insert('d7_upgrade_analysis_data')
        ->fields(array(
          'item_class' => $class,
          'item_type' => 'line-item',
          'data' => $json,
        ))
        ->execute();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }

  }

  /**
   * Get analysis data from the database.
   *
   * @return array
   */
  protected function getAnalysisData() {
    $class = get_class($this);
    $data = array();
    try {
      $q = db_select('d7_upgrade_analysis_data', 'i')
        ->fields('i')
        ->condition('item_class', $class, '=')
        ->condition('item_type', "line-item", '=')
        ->execute();

      $rows = $q->fetchAll();

      foreach ($rows as $row) {
        $data[] = json_decode($row->data);
      }
      return $data;
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

  /**
   * Custom table sort.
   *
   * @param array $rows
   *   Table rows to be sorted
   *
   * @param array $header
   *   Table header
   *
   * @param $flag
   *   Sort flag
   *
   * @return array
   *
   */
  protected function nonSqlSort($rows, $header, $flag = SORT_REGULAR) {
    $return = array();
    $order = tablesort_get_order($header);
    $sort = tablesort_get_sort($header);
    $column = $order['sql'];

    // Extract column data to be sorted into a temporary array.
    foreach ($rows as $row) {
      $temp[] = $row[$column];
    }

    // Sort the temporary array.
    if ($sort == 'asc') {
      asort($temp, $flag);
    }
    else {
      arsort($temp, $flag);
    }

    // Reconstruct the return array.
    foreach ($temp as $index => $data) {
      $return[] = $rows[$index];
    }

    return $return;
  }

}
