<?php

/**
 * @file
 * Definition of D7UpgradeAnalysisStatisticAbstract.
 */

/**
 * Class D7UpgradeAnalysisStatisticAbstract.
 */
abstract class D7UpgradeAnalysisStatisticAbstract {

  protected $id = NULL;

  public $label = NULL;

  public $description = NULL;

  protected $value = NULL;

  protected $analysisClass = NULL;

  public function __construct() {
    $this->id = $this->getId();
    $this->analysisClass = $this->getAnalysisClass();
    $this->label = $this->getLabel();
  }

  /**
   * Define a unique statistic identifier.
   *
   * Example:
   *  return 'my_unique_stat';
   *
   * @return string
   */
  abstract public function getId();

  /**
   * Define which analysis class this statistic belongs to.
   *
   * Example:
   *  return 'my_unique_stat';
   *
   * @return string
   */
  abstract public function getAnalysisClass();

  /**
   * Define a human readable label.
   *
   * Example:
   *  return 'My label';
   *
   * @return string
   */
  abstract public function getLabel();

  /**
   * Define a human readable description.
   *
   * Example:
   *  return t('My description');
   *
   * @return string
   */
  abstract public function getDescription();

  /**
   * Implement a value calculation.
   *
   * Example:
   *  $this->value = 1;
   *
   * @return mixed
   */
  abstract protected function calculateValue();

  /**
   * Method for executing stat calculation and saving data into the database.
   */
  public function calculate() {
    $this->calculateValue();
    $this->saveValue();
  }

  /**
   * Method for saving a calculated stat values in the database.
   */
  protected function saveValue() {
    $class = get_class($this);
    try {
      db_insert('d7_upgrade_analysis_data')
        ->fields(array(
          'item_type' => 'stat',
          'item_class' => $class,
          'data' => $this->value,
        ))
        ->execute();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

  /**
   * Retrieves a stored value calculation from the database.
   *
   * @return mixed
   */
  public function getStoredValue() {
    $class = get_class($this);
    try {
      $q = db_select('d7_upgrade_analysis_data', 'i')
        ->fields('i', array(
          'data'
        ))
        ->condition('item_class', $class, '=')
        ->condition('item_type', "stat", '=')
        ->execute();

      return $q->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }


  /**
   * Saves a reference to this class in the database for later use.
   */
  public function saveItem() {
    $class = get_class($this);
    try {
      db_insert('d7_upgrade_analysis_items')
        ->fields(array(
          'item_type' => 'stat',
          'item_class' => $class,
          'item_data_key' => $this->id,
        ))
        ->execute();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
