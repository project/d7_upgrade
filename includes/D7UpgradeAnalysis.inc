<?php

/**
 * @file
 * Class D7UpgradeAnalysis
 */

/**
 * Class D7UpgradeAnalysis.
 */
class D7UpgradeAnalysis {

  private $analyses = array();

  public function __construct() {
    $this->getAnalyses();
  }

  /**
   * Main settings form for the Upgrade Analysis.
   *
   * @return array
   */
  public function getForm() {
    $form = array();

    $analyses = $this->analyses;
    foreach ($analyses as $class) {
      $analysis = new $class;
      $labels[] = $analysis->getLabel();
    }

    if (isset($labels)) {
      $form['overview'] = array (
        '#theme' => 'item_list',
        '#items' => $labels,
        '#prefix' => t('<h2>The following analyses will be executed</h2>'),
      );

      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Run'),
      );
    }
    else {
      drupal_set_message(t('No Drupal 7 Upgrade analysis submodules are enabled. See modules starting "Drupal 7 Upgrade" on the <a href="@link">modules administration page</a>.',
        array(
          '@link' => url('admin/modules'),
        )
      ), 'status');
    }

    return $form;
  }

  /**
   * Handles submissions for the main Upgrade Analysis form.
   *
   * @param $form_state
   * @return null
   */
  public function submitForm(&$form_state) {
    // Clear existing data from the database.
    $this->clearAnalysisItems();
    variable_del('d7_upgrade_last_run');

    $analyses = $this->analyses;

    $operations[] = array('_d7_upgrade_batch_identify_items', array('data' => $analyses));
    $batch = array(
      'operations' => $operations,
      'finished' => '_d7_upgrade_batch_identify_items_finished',
      'title' => t('Drupal 7 Upgrade Analysis: Stage 1 of 2'),
      'init_message' => t('Starting'),
      'progress_message' => t('In progress'),
      'error_message' => t('An error has occurred.'),
    );
    batch_set($batch);

    return NULL;
  }

  /**
   * Implements hook_form().
   *
   * @return array
   */
  public function getAnalysisForm() {
    $form = array();
    $form['#attached']['js'][] = drupal_get_path('module', 'd7_upgrade') . '/js/d7_upgrade.js';
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Run'),
      '#attributes' => array(
        'class' => array('element-invisible'),
      ),
    );

    return $form;
  }

  /**
   * Implements hook_form_submit().
   *
   * @param $form_state
   * @return NULL
   */
  public function submitAnalysisForm(&$form_state) {
    // Clear existing data from the database.
    $this->clearAnalysisData();

    $items_q = db_select('d7_upgrade_analysis_items', 'i')
      ->fields('i')
      ->condition('item_type', 'line-item', '=')
      ->execute();

    $stats_q = db_select('d7_upgrade_analysis_items', 'i')
      ->fields('i')
      ->condition('item_type', 'stat', '=')
      ->execute();

    $items = $items_q->fetchAll();
    $stats = $stats_q->fetchAll();

    // Merge items and stats into one array.
    // Stats are processed at the end of the batch operation in case they need to use line-item data.
    $data = array_merge($items, $stats);

    $operations[] = array('_d7_upgrade_batch_analyse_items', array('data' => $data));
    $batch = array(
      'operations' => $operations,
      'finished' => '_d7_upgrade_batch_analyse_items_finished',
      'title' => t('Drupal 7 Upgrade Analysis: Stage 2 of 2'),
      'init_message' => t('Starting'),
      'progress_message' => t('In progress'),
      'error_message' => t('An error has occurred.'),
    );
    batch_set($batch);

    return NULL;
  }

  /**
   * Identify classes that implement upgrade analyses.
   */
  private function getAnalyses() {
    // Invoke custom hook_register_analysis_classes to identify registered
    // analysis classes in the codebase.
    $classes = module_invoke_all('register_analysis_classes');

    // Check each class exists and add it to the analyses array.
    foreach ($classes as $class) {
      if (class_exists($class)) {
        if (!in_array($class, $this->analyses)) {
          array_push($this->analyses, $class);
        }
      }
    }
  }

  /**
   * Render reports.
   * @return array
   */
  public function renderReports() {
    $form = array();

    // Identify when the analysis process last ran.
    $last_run = variable_get('d7_upgrade_last_run');
    if (!$last_run) {
      // If the process has never run, do not attempt to render analysis reports.
      return $form;
    }
    else {
      $form['timestamp'] = array (
        '#type' => 'markup',
        '#markup' => t('Last run @time ago', array('@time' => format_interval((time() - $last_run), 2))),
      );
    }

    $analyses = $this->analyses;
    foreach ($analyses as $class) {
      $analysis = new $class;
      $id = $analysis->getId();
      $report_url = url(D7_UPGRADE_SINGLE_REPORT_PATH . "/" . $class);
      $csv_url = url(D7_UPGRADE_SINGLE_CSV_PATH . "/" . $class);
      $stats_report = $analysis->renderStatsReport();

      $form[$id] = array(
        '#type' => 'fieldset',
        '#title' => t($analysis->getLabel()),
        '#value' => $stats_report,
      );
      $form[$id]['view_report'] = array(
        '#type' => 'link',
        '#title' => t('View full report'),
        '#href' => $report_url,
        '#attributes' => array(
          'class' => array(
            'button'
          )
        ),
      );
      $form[$id]['download_csv'] = array(
        '#type' => 'link',
        '#title' => t('Download full report (.csv)'),
        '#href' => $csv_url,
        '#attributes' => array(
          'class' => array(
            'button'
          )
        ),
      );
    }

    return $form;
  }

  /**
   * Get report.
   * @param $class
   *
   * @return array
   * @throws \Exception
   */
  public function getReport($class) {
    $analyses = $this->analyses;
    $form = array();

    // Identify when the analysis process last ran.
    $last_run = variable_get('d7_upgrade_last_run');
    if (!$last_run) {
      // If the process has never run, do not attempt to render analysis reports.
      $this->analysisNotRunWarning();
      return $form;
    }
    else {
      $form['timestamp'] = array (
        '#type' => 'markup',
        '#markup' => t('Last run @time ago', array('@time' => format_interval((time() - $last_run) , 2))),
      );
    }

    if (in_array($class, $analyses)) {
      $analysis = new $class;
      $id = $analysis->getId();

      $stats_report = $analysis->renderStatsReport();
      if (!empty($stats_report)) {
        $form[] = array(
          '#type' => 'fieldset',
          '#title' => t('Summary: @label', array('@label' => $analysis->getLabel())),
          '#value' => $stats_report,
        );
      }

      $report_data = $analysis->renderReport();

      if (!empty($report_data) && is_array($report_data) && array_key_exists('rows', $report_data) &&
        array_key_exists('header', $report_data)) {
        $report = theme('table', $report_data);
        $form[$id] = array(
          '#type' => 'fieldset',
          '#title' => $analysis->getLabel(),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#value' => $report,
        );
      }
      else {
        drupal_set_message(t('Unable to render report.'), 'error');
      }
    }

    return $form;
  }

  /**
   * Page callback to download a CSV for a given report class.
   *
   * @param $class
   * @return array
   */
  public function getReportCsv($class) {

    // Identify when the analysis process last ran.
    $last_run = variable_get('d7_upgrade_last_run');
    if (!$last_run) {
      // If the process has never run, display a warning message and return.
      $this->analysisNotRunWarning();
      drupal_goto(D7_UPGRADE_REPORT_PATH);
    }

    $analyses = $this->analyses;
    $form = array();

    if (in_array($class,$analyses)) {
      $analysis = new $class;
      $report_data = $analysis->renderReport();
      $id = $analysis->getId();

      if (!empty($report_data) && is_array($report_data) && array_key_exists('rows', $report_data) &&
        array_key_exists('header', $report_data)) {

        $csv = $this->generateCsvData($report_data['rows'], $report_data['header']);
        $filename = 'export-' . $id . '-report.csv';
        $file = $this->saveCsv($csv, $filename);
        $this->downloadFile($file, $filename);
      }
      else {
        drupal_set_message(t('Unable to generate CSV'), 'error');
      }
    }

    return $form;
  }

  /**
   * Page callback to all CSV files.
   *
   * @param $class
   * @return array
   */
  public function getAllCsv() {

    // Identify when the analysis process last ran.
    $last_run = variable_get('d7_upgrade_last_run');
    if (!$last_run) {
      // If the process has never run, display a warning message and return.
      $this->analysisNotRunWarning();
      drupal_goto(D7_UPGRADE_REPORT_PATH);
    }

    $analyses = $this->analyses;
    $files = array();

    foreach ($analyses as $analysis_class) {
      $analysis = new $analysis_class;
      $report_data = $analysis->renderReport();
      $id = $analysis->getId();


      if (!empty($report_data) && is_array($report_data) && array_key_exists('rows', $report_data) &&
        array_key_exists('header', $report_data)) {

        $csv = $this->generateCsvData($report_data['rows'], $report_data['header']);
        $filename = 'export-' . $id . '-report.csv';
        $file = $this->saveCsv($csv, $filename);
        $files[] = $file;
      }
      else {
        drupal_set_message("Unable to generate CSV.", "error");
      }
    }
    $zip_filename = "export-all-analysis-reports.zip";
    $this->zipFiles($files, $zip_filename);
  }

  /**
   * Delete all analysis items from the database.
   */
  public function clearAnalysisItems() {
    try {
      db_delete('d7_upgrade_analysis_items')->execute();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }

  }

  /**
   * Delete all analysis data from the database.
   */
  public function clearAnalysisData() {
    try {
      db_delete('d7_upgrade_analysis_data')->execute();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }

  }

  /**
   * Generates CSV data from an array of rows and headers for use in saveCsv.
   *
   * @param $rows
   * @param $headers
   * @return array
   */
  private function generateCsvData($rows, $headers) {
    $return_array = array();

    // Construct the first row from table headers.
    $header_row = array();
    foreach ($headers as $key => $value) {
      $header_row[] = $value['data'];
    }

    // Add header as the first row.
    $return_array[] = $header_row;

    // Add rows.
    foreach ($rows as $row => $values) {
      $return_array[] = $values;
    }

    return $return_array;
  }

  /**
   * Saves a CSV file into the tmp directory using data generated in generateCsvData.
   *
   * @param $data
   *  CSV data provided by generateCsvData.
   * @param $filename
   *  Output filename including the file extension.
   *
   * @return string
   *  Path to the saved file.
   */
  private function saveCsv($data, $filename) {
    $dir = file_directory_temp();
    $file = $dir . "/" . $filename;
    $handle = fopen($file, 'w');
    foreach ($data as $line) {
      fputcsv($handle, $line);
    }
    fclose($handle);
    return $file;
  }

  /**
   * Method to download a given CSV file.
   *
   * @param $file
   *  Path to a CSV file.
   * @param $filename
   *  Output filename for the CSV file including the file extension.
   */
  private function downloadFile($file, $filename) {
    header('Content-type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '"');
    readfile($file);
    drupal_exit();
  }

  /**
   * Method to generate and download a Zip file from an array of files.
   *
   * @param $files
   *  An array of file paths to be zipped together.
   * @param $filename
   *  Output filename for the Zip file including the file extension.
   */
  private function zipFiles($files, $filename) {
    $dir = file_directory_temp();
    $time = time();

    $zipfile = $dir . "/" . $time . "-" . $filename;

    $zip = new ZipArchive();
    $zip->open($zipfile, ZipArchive::CREATE);

    foreach ($files as $file) {
      // Remove directory structure from the file so that only the filename is used.
      $new_filename = substr($file, strrpos($file, '/') + 1);

      // Add the file to the archive.
      $zip->addFile($file, $new_filename);
    }
    $zip->close();

    // Download the zipfile.
    header('Content-type: application/zip');
    header('Content-disposition: attachment; filename="' . $time . "-" . $filename . '"');
    header('Content-Length: ' . filesize($zipfile));
    readfile($zipfile);
    drupal_exit();
  }

  /**
   * Displays a warning message when the analysis has not been run.
   */
  private function analysisNotRunWarning() {
    drupal_set_message(t('Please <a href="@link">run the analysis process</a> before attempting to view or download reports',
      array(
        '@link' => url(D7_UPGRADE_FORM_PATH),
      )
    ), 'warning');
  }

}
