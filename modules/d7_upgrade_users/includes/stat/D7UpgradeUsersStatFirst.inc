<?php

/**
 * @file
 * Definition of D7UpgradeUsersStatFirst
 */

/**
 * Class D7UpgradeUsersStatFirst.
 */
class D7UpgradeUsersStatFirst extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'first_user_created';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('First user created');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Date when the oldest user account was created');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeUsers';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $timestamp = db_query('SELECT created FROM {users} WHERE uid != 0 ORDER BY created ASC LIMIT 1')->fetchField();
      $this->value = format_string('@date (@time ago)', array(
        '@date' => format_date($timestamp, 'custom', 'j F Y'),
        '@time' => format_interval((time() - $timestamp), 2),
      ));
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
