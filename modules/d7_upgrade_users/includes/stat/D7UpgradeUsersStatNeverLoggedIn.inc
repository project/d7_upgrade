<?php

/**
 * @file
 * Definition of D7UpgradeUsersStatNeverLoggedIn.
 */

/**
 * Class D7UpgradeUsersStatNeverLoggedIn.
 */
class D7UpgradeUsersStatNeverLoggedIn extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_users_never_logged_in';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Users never logged in');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of users that have never logged in');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeUsers';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $this->value = db_query('SELECT COUNT(*) FROM {users} WHERE login =0')->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
