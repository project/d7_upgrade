<?php

/**
 * @file
 * Definition of D7UpgradeUsersStatActive.
 */

/**
 * Class D7UpgradeUsersStatActive.
 */
class D7UpgradeUsersStatActive extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_active_users';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Enabled user accounts');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of user accounts that are able to log in (users that are not blocked).');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeUsers';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $this->value = db_query('SELECT COUNT(*) FROM {users} WHERE status =1')->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
