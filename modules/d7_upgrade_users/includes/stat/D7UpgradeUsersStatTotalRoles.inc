<?php

/**
 * @file
 * Definition of D7UpgradeUsersStatTotalRoles.
 */

/**
 * Class D7UpgradeUsersStatTotalRoles.
 */
class D7UpgradeUsersStatTotalRoles extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_roles';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Roles');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of roles (excluding anonymous)');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeUsers';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      // Exclude anonymous user role from results count.
      $this->value = db_query('SELECT COUNT(*) FROM {role} WHERE rid !=1')->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
