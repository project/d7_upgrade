<?php

/**
 * @file
 * Definition of D7UpgradeUsersStatTotal.
 */

/**
 * Class D7UpgradeUsersStatTotal.
 */
class D7UpgradeUsersStatTotal extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_users';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Users');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of users');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeUsers';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $this->value = db_query('SELECT COUNT(*) FROM {users} ')->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
