<?php

/**
 * @file
 * Definition of D7UpgradeUsersStatBlocked.
 */

/**
 * Class D7UpgradeUsersStatBlocked.
 */
class D7UpgradeUsersStatBlocked extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_blocked_users';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Blocked user accounts');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of user accounts that are blocked from logging in');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeUsers';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $this->value = db_query('SELECT COUNT(*) FROM {users} WHERE status =0')->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
