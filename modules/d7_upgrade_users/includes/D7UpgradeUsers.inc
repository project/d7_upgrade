<?php

/**
 * @file
 * Definition of D7UpgradeUsers.
 */

/**
 * Class D7UpgradeUsers.
 */
class D7UpgradeUsers extends D7UpgradeAnalysisAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'users';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Users');
  }

  /**
   * @inheritDoc
   */
  public function identifyItems() {
    $ignore_anonymous = TRUE;

    $roles = user_roles($ignore_anonymous);

    foreach ($roles as $k => $v) {
      $items[] = $k;
    }

    $this->items = $items;
    $this->saveItems();
  }

  /**
   * @inheritDoc
   */
  public function analyseItem($item) {
    $rid = $item;
    $role = user_role_load($rid);
    $count = db_query('SELECT COUNT(*) FROM {users} u 
        INNER JOIN {users_roles} ur ON u.uid = ur.uid WHERE rid = :rid',
      array(':rid' => $rid))->fetchField();

    $this->analysisData[] = array(
      'id' => $rid,
      'role_name' => $role->name,
      'user_count' => $count,
    );

    $this->saveAnalysisData();
  }

  /**
   * @inheritDoc
   */
  public function renderReport() {
    $data = $this->getAnalysisData();

    $header = array();
    $header[] = array('data' => t('Role ID'), 'field' => 'rid');
    $header[] = array('data' => t('Role name'), 'field' => 'name');
    $header[] = array('data' => t('User count'), 'field' => 'user_count', 'sort' => 'desc');

    $rows = array();
    foreach ($data as $k => $v) {
      foreach ($v as $item) {
        $rows[] = array (
          'rid' => $item->id,
          'name' => $item->role_name,
          'user_count' => $item->user_count
        );
      }
    }

    $rows = $this->nonSqlSort($rows, $header);

    $return = array(
      'header' => $header,
      'rows' => $rows,
    );

    return $return;
  }

}
