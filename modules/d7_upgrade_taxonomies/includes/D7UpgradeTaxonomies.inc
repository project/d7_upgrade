<?php

/**
 * @file
 * Definition of D7UpgradeTaxonomies.
 */

/**
 * Class D7UpgradeTaxonomies.
 */
class D7UpgradeTaxonomies extends D7UpgradeAnalysisAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'taxonomies';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Taxonomies');
  }

  /**
   * @inheritDoc
   */
  public function identifyItems() {
    $taxonomies = taxonomy_get_vocabularies();
    foreach ($taxonomies as $taxonomy) {
      $this->items[] = $taxonomy->vid;
    }
    $this->saveItems();
  }

  /**
   * @inheritDoc
   */
  public function analyseItem($item) {
    $vid = $item;
    $vocabulary = taxonomy_vocabulary_load($vid);
    $this->analysisData['key'] = $vid;
    $this->analysisData['machine_name'] = $vocabulary->machine_name;
    $this->analysisData['name'] = $vocabulary->name;
    $this->analysisData['module'] = $vocabulary->module;

    $terms = db_select('taxonomy_term_data', 'v')
      ->fields('v')
      ->condition('vid', $vid. '=')
      ->execute();
    $this->analysisData['terms'] = $terms->rowCount();

    $this->saveAnalysisData();
  }

  /**
   * @inheritDoc
   */
  public function renderReport() {
    $data = $this->getAnalysisData();

    $header = array();
    $header[] = array('data' => t('Vid'), 'field' => 'vid');
    $header[] = array('data' => t('Name'), 'field' => 'name');
    $header[] = array('data' => t('Machine name'), 'field' => 'machine_name');
    $header[] = array('data' => t('Module'), 'field' => 'module');
    $header[] = array('data' => t('Terms count'), 'field' => 'terms', 'sort' => 'DESC');

    $rows = array();
    foreach ($data as $k => $v) {
      $rows[] = array (
        'vid' => $v->key,
        'name' => $v->name,
        'machine_name' => $v->machine_name,
        'module' => $v->module,
        'terms' => $v->terms,
      );
    }

    $rows = $this->nonSqlSort($rows, $header);

    $return = array(
      'header' => $header,
      'rows' => $rows,
    );
    return $return;
  }

}
