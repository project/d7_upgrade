<?php

/**
 * @file
 * Definition of D7UpgradeTaxonomiesStatTotalVocabularies.
 */

/**
 * Class D7UpgradeTaxonomiesStatTotalVocabularies.
 */
class D7UpgradeTaxonomiesStatTotalVocabularies extends D7UpgradeAnalysisStatisticAbstract {

  public function getId() {
    return 'number_of_vocabularies';
  }

  public function getLabel() {
    return t('Vocabularies');
  }

  public function getDescription() {
    return t('Count of all taxonomy vocabularies');
  }

  public function getAnalysisClass() {
    return 'D7UpgradeTaxonomies';
  }

  protected function calculateValue() {
    try {
      $result = db_select('taxonomy_vocabulary', 'v')
        ->fields('v')
        ->execute();
      $this->value = $result->rowCount();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}