<?php

/**
 * @file
 * Definition of D7UpgradeTaxonomieStatTotalTerms.
 */

/**
 * Class D7UpgradeTaxonomiesStatTotalTerms.
 */
class D7UpgradeTaxonomiesStatTotalTerms extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_terms';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Terms');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of all taxonomy terms');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeTaxonomies';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $result = db_select('taxonomy_term_data', 'v')
        ->fields('v')
        ->execute();
      $this->value = $result->rowCount();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
