<?php

/**
 * @file
 * Definition of D7UpgradeModulesStatDisabled.
 */

/**
 * Class D7UpgradeModulesStatDisabled.
 */
class D7UpgradeModulesStatDisabled extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'disabled_modules';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Disabled');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of disabled modules');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeModules';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $result = db_select('system', 's')
        ->fields('s')
        ->condition('s.type', 'module', '=')
        ->condition('s.status', '0', '=')
        ->execute();
      $this->value = $result->rowCount();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
