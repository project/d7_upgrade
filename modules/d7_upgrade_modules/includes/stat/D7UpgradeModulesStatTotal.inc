<?php

/**
 * @file
 * Definition of D7UpgradeModulesStatTotal.
 */

/**
 * Class D7UpgradeModulesStatTotal.
 */
class D7UpgradeModulesStatTotal extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_modules';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Modules');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of all modules');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeModules';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $result = db_select('system', 's')
        ->fields('s')
        ->condition('s.type', 'module', '=')
        ->execute();
      $this->value = $result->rowCount();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
