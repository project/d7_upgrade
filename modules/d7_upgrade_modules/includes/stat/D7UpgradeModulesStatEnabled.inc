<?php

/**
 * @file
 * Definition of D7UpgradeModulesStatEnabled.
 */

/**
 * Class D7UpgradeModulesStatEnabled.
 */
class D7UpgradeModulesStatEnabled extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'enabled_modules';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Enabled');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of enabled modules');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeModules';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $result = db_select('system', 's')
        ->fields('s')
        ->condition('s.type', 'module', '=')
        ->condition('s.status', '1', '=')
        ->execute();
      $this->value = $result->rowCount();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
