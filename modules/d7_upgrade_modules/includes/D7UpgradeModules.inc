<?php

/**
 * @file
 * Definition of D7UpgradeModule
 */

/**
 * Class D7UpgradeModules.
 */
class D7UpgradeModules extends D7UpgradeAnalysisAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'modules';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Modules');
  }

  /**
   * @inheritDoc
   */
  public function identifyItems() {
    $this->items = module_list();
    $this->saveItems();
  }

  /**
   * @inheritDoc
   */
  public function analyseItem($item) {
    $module = system_get_info('module', $item);
    $this->analysisData['key'] = $item;
    if (isset($module['project'])) {
      $this->analysisData['project'] = $module['project'];
    }
    else {
      $this->analysisData['project'] = 'N/A';
    }
    $this->analysisData['human_name'] = $module['name'];
    $this->analysisData['version'] = $module['version'];
    $this->analysisData['package'] = $module['package'];
    $this->saveAnalysisData();
  }

  /**
   * @inheritDoc
   */
  public function renderReport() {
    $data = $this->getAnalysisData();

    $header = array();
    $header[] = array('data' => t('Module name'), 'field' => 'module_name', 'sort' => 'desc');
    $header[] = array('data' => t('Machine name'), 'field' => 'machine_name');
    $header[] = array('data' => t('Version'), 'field' => 'version');
    $header[] = array('data' => t('Project'), 'field' => 'project');
    $header[] = array('data' => t('Package'), 'field' => 'package');

    $rows = array();
    foreach ($data as $k => $v) {
      $rows[] = array (
        'module_name' => $v->human_name,
        'machine_name' => $v->key,
        'version' => $v->version,
        'project' => $v->project,
        'package' => $v->package,
      );
    }

    $rows = $this->nonSqlSort($rows, $header);

    $return = array(
      'header' => $header,
      'rows' => $rows,
    );

    return $return;
  }

}
