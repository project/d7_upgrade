<?php
/**
 * Class D7UpgradeViews.
 */

class D7UpgradeViews extends D7UpgradeAnalysisAbstract {

  public function getId() {
    return 'views';
  }

  public function getLabel() {
    return 'Views';
  }

  public function identifyItems() {
    $items = array();
    
    if (module_exists('views')) {
      $views = views_get_all_views();
      foreach ($views AS $view => $attribute) {
        $items[] = $view;
      }
    }
    
    $this->items = $items;
    $this->saveItems();
  }

  public function analyseItem($item) {
    $view = $item;
    $this->analysisData['key'] = $view;
    $view_object = views_get_view($view);
    $this->analysisData['human_name'] = $view_object->human_name;

    // Calculate number of displays.
    $this->analysisData['display_count'] = count($view_object->display);

    // Extract additional information about displays.
    $this->analysisData['displays'] = array();
    foreach ($view_object->display AS $display) {
      $id = $display->id;
      $enabled = 1;
      if (isset($display->display_options['enabled']) && $display->display_options['enabled'] == false) {
        $enabled = 0;
      }
      $this->analysisData['displays'][$id] = array(
        'title' => $display->display_title,
        'enabled' => $enabled
      );
    }

    $this->saveAnalysisData();
  }

  public function renderReport() {
    $data = $this->getAnalysisData();

    $header = array();
    $header[] = array('data' => t('Name'), 'field' => 'name', 'sort' => 'desc');
    $header[] = array('data' => t('Machine name'), 'field' => 'machine_name');
    $header[] = array('data' => t('Displays count'), 'field' => 'displays_count');
    $header[] = array('data' => t('Displays'), 'field' => 'displays');

    $rows = array();
    foreach ($data as $k => $v) {
      $displays_text = array();
      foreach ($v->displays as $display => $attribute) {
        $displays_text[] = $attribute->enabled == TRUE ? $display : $display . " (disabled)";
      }
      $displays_inline = implode(', ', $displays_text);

      $rows[] = array (
        'name' => $v->human_name,
        'machine_name' => $v->key,
        'displays_count' => $v->display_count,
        'displays' => $displays_inline
      );
    }

    $rows = $this->nonSqlSort($rows, $header);

    $return =  array(
      'header' => $header,
      'rows' => $rows,
    );

    return $return;
  }

}
