<?php

/**
 * @file
 * Definition of D7UpgradeViewsStatTotal.
 */

/**
 * Class D7UpgradeViewsStatTotal.
 */
class D7UpgradeViewsStatTotal extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_views';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Views');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of views');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeViews';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $views = [];

      if (module_exists('views')) {
        $views = views_get_all_views();
      }
    
      $this->value = count($views);
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
