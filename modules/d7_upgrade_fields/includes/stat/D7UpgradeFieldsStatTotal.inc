<?php

/**
 * @file
 * Definition of D7UpgradeFieldsStatTotal.
 */

/**
 * Class D7UpgradeFieldsStatTotal.
 */
class D7UpgradeFieldsStatTotal extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_fields';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Number of fields');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of all fields');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeFields';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $this->value = db_query('SELECT COUNT(*) FROM {field_config} ')->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
