<?php

/**
 * @file
 * Definition of D7UpgradeFields.
 */

/**
 * Class D7UpgradeFields.
 */
class D7UpgradeFields extends D7UpgradeAnalysisAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'fields';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return 'Fields';
  }

  /**
   * @inheritDoc
   */
  public function identifyItems() {
    $fields = field_info_fields();
    $items = array();
    foreach ($fields as $field => $attribute) {
      $items[] = $field;
    }
    $this->items = $items;
    $this->saveItems();
  }

  /**
   * @inheritDoc
   */
  public function analyseItem($item) {
    $field = $item;
    $this->analysisData['key'] = $field;
    $field_object = field_info_field($field);

    $this->analysisData['module'] = $field_object['module'];
    $this->analysisData['bundles'] = $field_object['bundles'];
    $this->analysisData['type'] = $field_object['type'];
    $this->analysisData['bundle_count'] = count($field_object['bundles']);
    $this->analysisData['bundle_item_count'] = 0;

    foreach ($field_object['bundles'] as $bundle => $bundle_items) {
      $this->analysisData['bundle_item_count'] += count($bundle_items);
    }

    $this->saveAnalysisData();
  }

  /**
   * @inheritDoc
   */
  public function renderReport() {
    $data = $this->getAnalysisData();

    $header = array();
    $header[] = array('data' => t('Field name'), 'field' => 'field_name', 'sort' => 'asc');
    $header[] = array('data' => t('Module'), 'field' => 'module');
    $header[] = array('data' => t('Type'), 'field' => 'type');
    $header[] = array('data' => t('Bundle count'), 'field' => 'bundle_count');
    $header[] = array('data' => t('Bundle item count'), 'field' => 'bundle_item_count');
    $header[] = array('data' => t('Used in'), 'field' => 'used_in');

    $rows = array();
    foreach ($data as $k => $v) {
      $bundle_data = '';
      foreach ($v->bundles as $bundle => $bundle_items) {
        $bundle_data .= "<h3>" . $bundle . "</h3>";
        $bundle_data .= implode(', ', $bundle_items);
      }

      $rows[] = array (
        'field_name' => $v->key,
        'module' => $v->module,
        'type' => $v->type,
        'bundle_count' => $v->bundle_count,
        'bundle_item_count' => $v->bundle_item_count,
        'used_in' => $bundle_data
      );
    }

    $rows = $this->nonSqlSort($rows, $header);

    $return = array(
      'header' => $header,
      'rows' => $rows,
    );

    return $return;
  }

}
