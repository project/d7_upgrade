<?php

/**
 * @file
 * Definition of D7UpgradeContentTypesStatTotal
 */

/**
 * Class D7UpgradeContentTypesStatTotal.
 */
class D7UpgradeContentTypesStatTotal extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_content_types';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Number of content types');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of all content (node) types');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeContentTypes';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $this->value = db_query('SELECT COUNT(*) FROM {node_type}')->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
