<?php

/**
 * @file
 * Definition of D7UpgradeContentTypesStatTotalNodes.
 */

/**
 * Class D7UpgradeContentTypesStatTotalNodes.
 */
class D7UpgradeContentTypesStatTotalNodes extends D7UpgradeAnalysisStatisticAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'number_of_nodes';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Number of nodes');
  }

  /**
   * @inheritDoc
   */
  public function getDescription() {
    return t('Count of all nodes');
  }

  /**
   * @inheritDoc
   */
  public function getAnalysisClass() {
    return 'D7UpgradeContentTypes';
  }

  /**
   * @inheritDoc
   */
  protected function calculateValue() {
    try {
      $this->value = db_query('SELECT COUNT(*) FROM {node}')->fetchField();
    }
    catch (\Exception $exception) {
      watchdog_exception('type', $exception);
    }
  }

}
