<?php

/**
 * @file
 * Definition of D7UpgradeContentTypes.
 */

/**
 * Class D7UpgradeContentTypes.
 */
class D7UpgradeContentTypes extends D7UpgradeAnalysisAbstract {

  /**
   * @inheritDoc
   */
  public function getId() {
    return 'content_types';
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return t('Content Types');
  }

  /**
   * @inheritDoc
   */
  public function identifyItems() {
    $content_types = node_type_get_types();
    $items = array();
    foreach ($content_types as $content_type => $attribute) {
      $items[] = $content_type;
    }
    $this->items = $items;
    $this->saveItems();
  }

  /**
   * @inheritDoc
   */
  public function analyseItem($item) {
    $content_type = $item;
    $this->analysisData['key'] = $content_type;
    $this->analysisData['human_name'] = node_type_get_name($content_type);

    // Calculate total instances.
    $this->analysisData['count'] = db_query('SELECT COUNT(*) FROM {node} WHERE type = :type', array(':type' => $content_type))->fetchField();

    // Get fields.
    $fields = field_info_instances('node', $content_type);
    $this->analysisData['field_count'] = count($fields);

    $this->saveAnalysisData();
  }

  /**
   * @inheritDoc
   */
  public function renderReport() {
    $data = $this->getAnalysisData();

    $header = array();
    $header[] = array('data' => t('Content type'), 'field' => 'content_type', 'sort' => 'desc');
    $header[] = array('data' => t('Machine name'), 'field' => 'machine_name');
    $header[] = array('data' => t('Node count'), 'field' => 'node_count');
    $header[] = array('data' => t('Field count'), 'field' => 'field_count');

    $rows = array();
    foreach ($data as $k => $v) {
      $rows[] = array (
        'content_type' => $v->human_name,
        'machine_name' => $v->key,
        'node_count' => $v->count,
        'field_count' => $v->field_count
      );
    }

    $rows = $this->nonSqlSort($rows, $header);

    $return = array(
      'header' => $header,
      'rows' => $rows,
    );

    return $return;
  }

}
